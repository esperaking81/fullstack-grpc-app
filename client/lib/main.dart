import 'dart:math';

import 'package:flutter/material.dart';
import 'package:protos/protos.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatefulWidget {
  const MainApp({super.key});

  @override
  State<MainApp> createState() => _MainAppState();
}

class _MainAppState extends State<MainApp> {
  late ClientChannel _channel;

  late TodoServiceClient _stub;

  final _sampleRequest = GetTodoByIdRequest(id: 1);

  Todo? _todo;

  Stream<Todo>? _todoStream;

  @override
  void initState() {
    super.initState();

    _channel = ClientChannel(
      'localhost',
      port: 1337,
      options: const ChannelOptions(
        credentials: ChannelCredentials.insecure(),
      ),
    );

    _stub = TodoServiceClient(_channel);

    _todoStream = _stub.getTodoStream(_sampleRequest);
  }

  void _getTodo() async {
    final todo = await _stub.getTodo(_sampleRequest);
    setState(() {
      _todo = todo;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: SizedBox.expand(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              StreamBuilder(
                stream: _todoStream,
                builder: (context, AsyncSnapshot<Todo> snapshot) {
                  if (snapshot.hasData) {
                    final todo = snapshot.data as Todo;
                    return Column(
                      children: [
                        Text(todo.id.toString()),
                        Text(todo.title),
                        Text(todo.completed.toString()),
                      ],
                    );
                  }

                  return const Text('Loading...');
                },
              ),
              // if (_todo != null) ...[
              //   Text(_todo!.id.toString()),
              //   Text(_todo!.title),
              //   Text(_todo!.completed.toString()),
              //   const SizedBox(height: 16),
              //   ElevatedButton(
              //     onPressed: _getTodo,
              //     child: const Text('Get new todo'),
              //   )
              // ] else ...[
              //   ElevatedButton(
              //     onPressed: _getTodo,
              //     child: const Text('Get todo'),
              //   )
              // ],
            ],
          ),
        ),
      ),
    );
  }
}
